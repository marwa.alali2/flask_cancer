#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import joblib
# %matplotlib inline
import numpy as np
# import matplotlib.pyplot as plt
# import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn import tree

from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier


from sklearn.ensemble import  RandomForestClassifier

from flask import Flask, request , render_template
import numpy as np
#import joblib
import pickle
app = Flask(__name__)
model_1 = pickle.load(open ('model_1.sav', 'rb') )
model_2 = pickle.load(open ('model_2.sav', 'rb') )
model_3 = pickle.load(open ('model_3.sav', 'rb') )

@app.route('/')
def home():          
    return render_template('index.html')

@app.route('/predict',methods=['POST'])

def predict():            
    data_saisie = [int (x) for x in request.form.values()]
    model_choisi = data_saisie[-1]
    data_saisie = data_saisie[:-1]
     
    print("data saisi",data_saisie)
    print(model_choisi)
    X = [ np.array(data_saisie)]
   
    print(X)
    if model_choisi == 1:
        print("1")
        prediction = model_1.predict(X)
        score_model = "0.95%"
    elif  model_choisi == 2:
        print("2")
        prediction = model_2.predict(X)
        score_model = "0.97%"
    else :
        print("3")       
        prediction = model_3.predict(X)
        score_model ="0.96%"
    cancer_pred = round (prediction [0], 2)
    if cancer_pred == 2:
        result = "Bénigne"
    else:
        result = "Maligne"
    return render_template ('index.html', 
    zone_prediction ="La cellule est : {}, avec un score d'apprentissage de {}".format(result,score_model))
if __name__ == "__main__":
    app.run(debug = True)




# ### 
